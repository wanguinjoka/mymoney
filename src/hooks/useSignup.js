import { useState, useEffect  } from 'react'
import { projectAuth } from '../firebase/config'
import { useAuthContext } from './useAuthContext'

// hook used to register users in firebase

export const useSignup = () => {
    const [isCancelled, setIsCancelled] = useState(null)
    const [error, setError] = useState(null)
    const [isPendng, setIsPending] = useState(false)
    const { dispatch } = useAuthContext()

    const signup = async (email, password, displayName) => {
        setError(null)
        setIsPending(true)

        try{
            //signup user
            const res = await projectAuth.createUserWithEmailAndPassword(email, password)
            // console.log(res.user)
            if(!res){
                throw new Error('Could not complete signup')
            }
            // add display name to user
            await res.user.updateProfile({ displayName: displayName})
            // dispatch login action after signup and whenever
            dispatch({ type: 'LOGIN', payload: res.user })
            if(!isCancelled){
                setIsPending(false)
                setError(null)
            }
        }
        catch (err){
            if(!isCancelled){
                console.log(err.message)
                setError(err.message)
                setIsPending(false)
            }
        } 
    } 
    useEffect(() => {
        return () => {
          setIsCancelled(true)
        }
      }, [])
    return { error, isPendng, signup } 

}