import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

const firebaseConfig = {
    apiKey: "AIzaSyCzlfHMC7Gb-IT32r0ubANbqEHzPskhJrw",
    authDomain: "mymoney-18aa6.firebaseapp.com",
    projectId: "mymoney-18aa6",
    storageBucket: "mymoney-18aa6.appspot.com",
    messagingSenderId: "192259814504",
    appId: "1:192259814504:web:2a666822d0b4bca48df7dd"
  }

  firebase.initializeApp(firebaseConfig)

  const projectFirestore = firebase.firestore()
  const projectAuth = firebase.auth()

  // timestamp
  const timestamp = firebase.firestore.Timestamp

  export { projectFirestore, projectAuth, timestamp }